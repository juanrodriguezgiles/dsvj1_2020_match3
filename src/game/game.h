#ifndef GAME_H
#define GAME_H
#include "raylib.h"
#include "scenes/main_menu/main_menu.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/game_over/game_over.h"
#include "scenes/pause/pause.h"
#include "scenes/exit/exit.h"
#include "scenes/credits/credits.h"
#include "scenes/how_to_play/how_to_play.h"
namespace match3
{
	namespace game
	{
		enum class scene
		{
			main_menu,
			credits,
			gameplay,
			pause,
			exitGame,
			game_over,
			how_to_play
		};
		extern scene currentScene;
		extern bool playing;
		extern double time;

		void run();
		void init();
		void deInit();
	}
}
#endif