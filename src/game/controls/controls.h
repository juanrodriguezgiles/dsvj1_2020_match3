#ifndef CONTROLS_H
#define CONTROLS_H
#include "raylib.h"
namespace endless
{
	namespace controls
	{
		extern int left;
		extern int right;
		extern int pauseGame;
	}
}
#endif
