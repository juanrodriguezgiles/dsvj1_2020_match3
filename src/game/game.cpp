#include "game.h"
#include "raylib.h"
namespace match3
{
	namespace game
	{
		scene currentScene = scene::main_menu;
		bool playing = true;
		double time;

		void init()
		{
			switch (currentScene)
			{
			case match3::game::scene::main_menu:
				match3::main_menu::init();
				break;
			case match3::game::scene::credits:
				match3::credits::init();
				break;
			case match3::game::scene::gameplay:
				match3::gameplay::init();
				break;
			case match3::game::scene::pause:
				match3::pause::init();
				break;
			case match3::game::scene::exitGame:
				match3::exitGame::init();
				break;
			case match3::game::scene::game_over:
				match3::game_over::init();
				break;
			case match3::game::scene::how_to_play:
				match3::how_to_play::init();
				break;
			default:
				break;
			}
		}
		static void input()
		{
			switch (currentScene)
			{
			case match3::game::scene::main_menu:
				match3::main_menu::input();
				break;
			case match3::game::scene::credits:
				match3::credits::input();
				break;
			case match3::game::scene::gameplay:
				match3::gameplay::input();
				break;
			case match3::game::scene::pause:
				match3::pause::input();
				break;
			case match3::game::scene::exitGame:
				match3::exitGame::input();
				break;
			case match3::game::scene::game_over:
				match3::game_over::input();
				break;
			case match3::game::scene::how_to_play:
				match3::how_to_play::input();
				break;
			default:
				break;
			}
		}
		static void update()
		{
			switch (currentScene)
			{
			case match3::game::scene::main_menu:
				match3::main_menu::update();
				break;
			case match3::game::scene::credits:
				match3::credits::update();
				break;
			case match3::game::scene::gameplay:
				match3::gameplay::update();
				break;
			case match3::game::scene::pause:
				match3::pause::update();
				break;
			case match3::game::scene::exitGame:
				match3::exitGame::update();
				break;
			case match3::game::scene::game_over:
				match3::game_over::update();
				break;
			case match3::game::scene::how_to_play:
				match3::how_to_play::update();
				break;
			default:
				break;
			}
		}
		static void draw()
		{
			switch (currentScene)
			{
			case match3::game::scene::main_menu:
				match3::main_menu::draw();
				break;
			case match3::game::scene::credits:
				match3::credits::draw();
				break;
			case match3::game::scene::gameplay:
				match3::gameplay::draw();
				break;
			case match3::game::scene::pause:
				match3::pause::draw();
				break;
			case match3::game::scene::exitGame:
				match3::exitGame::draw();
				break;
			case match3::game::scene::game_over:
				match3::game_over::draw();
				break;
			case match3::game::scene::how_to_play:
				match3::how_to_play::draw();
				break;
			default:
				break;
			}
		}
		static void unLoadTextures()
		{
			UnloadTexture(gem::blue);
			UnloadTexture(gem::gray);
			UnloadTexture(gem::orange);
			UnloadTexture(gem::red);
			UnloadTexture(gameplay::movesLeftT);
			UnloadTexture(gameplay::score);
			UnloadTexture(gameplay::highScore);
			UnloadTexture(screen::backgroundGameplay);
			UnloadTexture(screen::backgroundMainMenu);
			

		}
		static void deInit()
		{
			main_menu::deInit();
			pause::deInit();
			gameplay::deInit();
			game_over::deInit();
			exitGame::deInit();
			how_to_play::deInit();
			credits::deInit();
			unLoadTextures();
			CloseAudioDevice();
			CloseWindow();
		}
		void run()
		{
			while (!WindowShouldClose() && playing)
			{
				input();
				update();
				draw();
			}
			deInit();
		}
	}
}