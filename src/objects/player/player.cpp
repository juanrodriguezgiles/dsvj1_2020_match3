#include "player.h"
namespace match3
{
	namespace player
	{
		Vector2 mousePos;
		bool tagSelecting;
		bool tagMovingGems;
		bool tagDespawning;
		bool firstIteration;
		int despawnAnimationCounter;
		int playerScore;
		int playerHighScore = 0;
		int movesLeft;

		void initPlayer()
		{
			mousePos = { 0,0 };
			tagSelecting = false;
			tagMovingGems = false;
			tagDespawning = false;
			firstIteration = true;
			despawnAnimationCounter = 0;
			playerScore = 0;
			movesLeft = 25;
		}
		void updateMousePos()
		{
			mousePos = GetMousePosition();
		}
	}
}