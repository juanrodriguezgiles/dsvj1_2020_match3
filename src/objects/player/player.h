#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
namespace match3
{
	namespace player
	{
		extern Vector2 mousePos;
		extern bool tagSelecting;
		extern bool tagMovingGems;
		extern bool tagDespawning;
		extern bool firstIteration;
		extern int despawnAnimationCounter;
		extern int playerScore;
		extern int playerHighScore;
		extern int movesLeft;
		void initPlayer();
		void updateMousePos();
	}
}
#endif