#include "board.h"
#include <iostream>
using namespace match3;
using namespace gem;
using namespace player;
namespace match3
{
	namespace board
	{
		list <Vector2> selectedGems;
		static int emptySpaces = 0;
		Sound match;
		Sound noMatch;

		static void checkGameOver()
		{
			if (movesLeft == 0)
			{
				game::currentScene = match3::game::scene::game_over;
				game::init();
			}
		}
		static void checkEmptySpaces(int row, int column)
		{
			for (int i = row + 1; i < rows; i++)
			{
				if (boardObject[i][column].type == gemType::noType)
				{
					emptySpaces++;
				}
				else
				{
					return;
				}
			}
		}
		static void swapGems(int row, int column)
		{
			checkEmptySpaces(row, column);

			gemObject gem = boardObject[row][column];;
			gemObject gemEmpty = boardObject[row + emptySpaces][column];

			match3::gem::boardObject[row][column].type = gemEmpty.type;
			match3::gem::boardObject[row][column].texture = gemEmpty.texture;

			match3::gem::boardObject[row][column].tagSelected = gemEmpty.tagSelected;
			match3::gem::boardObject[row][column].tagHover = gemEmpty.active;
			match3::gem::boardObject[row][column].active = gemEmpty.active;
			match3::gem::boardObject[row][column].tagMoving = gemEmpty.tagMoving;

			match3::gem::boardObject[row + emptySpaces][column].type = gem.type;
			match3::gem::boardObject[row + emptySpaces][column].texture = gem.texture;

			match3::gem::boardObject[row + emptySpaces][column].tagSelected = gem.tagSelected;
			match3::gem::boardObject[row + emptySpaces][column].tagHover = gem.tagHover;
			match3::gem::boardObject[row + emptySpaces][column].active = gem.active;
			match3::gem::boardObject[row + emptySpaces][column].tagMoving = true;
			match3::gem::boardObject[row + emptySpaces][column].body = gem.body;
			match3::gem::boardObject[row + emptySpaces][column].newY = int(gemEmpty.body.y);

			emptySpaces = 0;
		}
		static void addGems()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					if (boardObject[i][j].type == gemType::noType)
						rePopulateGem(boardObject[i][j], i, j);
				}
			}
			firstIteration = false;
			tagMovingGems = true;
		}
		static void moveGems()
		{
			tagMovingGems = false;
			for (int i = rows - 1; i >= 0; i--)
			{
				for (int j = 0; j < columns; j++)
				{
					if (boardObject[i][j].body.y < boardObject[i][j].newY)
					{
						boardObject[i][j].body.y += 0.2f;
						tagMovingGems = true;
					}
					else
						boardObject[i][j].tagMoving = false;
				}
			}
			if (!tagMovingGems && firstIteration)
				addGems();
			if (!tagMovingGems && !firstIteration)
				checkGameOver();
		}
		static void removeGems()
		{
			static Vector2 index;
			static list <Vector2>::iterator pos;
			pos = selectedGems.begin();

			while (pos != selectedGems.end())
			{
				index = *pos;
				pos++;
				boardObject[int(index.y)][int(index.x)].type = gemType::noType;
				boardObject[int(index.y)][int(index.x)].active = false;
			}

			selectedGems.clear();
			tagSelecting = false;
			tagDespawning = true;
			firstIteration = true;
		}
		static void updateScore()
		{
			playerScore += selectedGems.size() * 100;
			if (playerScore >= playerHighScore)
				playerHighScore = playerScore;
			movesLeft--;
		}
		void animateGems()
		{
			for (int i = rows - 2; i >= 0; i--)
			{
				for (int j = 0; j < columns; j++)
				{
					if (boardObject[i][j].type != gemType::noType && boardObject[i + 1][j].type == gemType::noType)
					{
						swapGems(i, j);
					}
				}
			}
		}
		void createBoard()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					createGem(boardObject[i][j], i, j);
				}
			}
		}
		void updateBoard()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					if (!tagMovingGems && !tagDespawning)
					{
						checkHoverGem(boardObject[i][j]);
						checkSelectGem(boardObject[i][j], i, j);
						if (tagSelecting)
							checkAdjacentGem(boardObject[i][j], i, j);
					}
					else if (tagMovingGems && !tagDespawning)
						moveGems();
					else if (!tagMovingGems && tagDespawning)
						despawnAnimation();
				}
			}
		}
		void drawBoard()
		{
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					if (boardObject[i][j].type != gemType::noType)
						drawGem(boardObject[i][j]);
					else if (tagDespawning)
						drawGemDespawn(boardObject[i][j]);
				}
			}
		}
		void deSelectBoard()
		{
			static Vector2 index;
			static list <Vector2>::iterator pos;
			pos = selectedGems.begin();

			while (pos != selectedGems.end())
			{
				index = *pos;
				pos++;
				boardObject[int(index.y)][int(index.x)].tagSelected = false;
			}
			tagSelecting = false;
			selectedGems.clear();
		}
		void checkMatch()
		{
			static Vector2 index;
			static list <Vector2>::iterator pos;
			pos = selectedGems.begin();

			if (selectedGems.size() >= 3)
			{
				while (pos != selectedGems.end())
				{
					index = *pos;
					pos++;
					boardObject[int(index.y)][int(index.x)].tagSelected = false;
					boardObject[int(index.y)][int(index.x)].active = false;
				}
				PlaySound(match);
				updateScore();
				removeGems();
			}
			else
				PlaySound(noMatch);
		}
	}
}