#ifndef BOARD_H
#define BOARD_H
#include <list>
#include "raylib.h"
#include "objects/gem/gem.h"
#include "objects/player/player.h"
using namespace std;
namespace match3
{
	namespace board
	{
		extern list <Vector2> selectedGems;
		extern Sound match;
		extern Sound noMatch;

		void animateGems();
		void createBoard();
		void updateBoard();
		void drawBoard();
		void deSelectBoard();
		void checkMatch();
	}
}
#endif

