#ifndef GEM_H
#define GEM_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
#include "objects/board/board.h"
namespace match3
{
	namespace gem
	{
		enum class gemType
		{
			blue,
			red,
			orange,
			gray,
			noType
		};
		struct gemObject
		{
			gemType type = gemType::noType;
			Texture2D texture;
			Rectangle body = { 0,0,0,0 };
			int newY = 0;
			bool tagSelected = false;
			bool tagHover = false;
			bool tagMoving = false;
			bool active = true;
		};
		extern const int columns;
		extern const int rows;
		extern gemObject boardObject[8][7];

		extern Texture2D blue;
		extern Texture2D red;
		extern Texture2D orange;
		extern Texture2D gray;

		void loadGemTextures();
		void createGem(gemObject& gem, int row, int column);
		void rePopulateGem(gemObject& gem, int row, int column);
		void drawGem(gemObject gem);
		void drawGemDespawn(gemObject gem);
		void checkHoverGem(gemObject& gem);
		void checkSelectGem(gemObject& gem, int row, int column);
		void checkAdjacentGem(gemObject& gem, int row, int column);
		void despawnAnimation();
	}
}
#endif