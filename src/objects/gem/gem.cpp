#include "gem.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace player;
using namespace board;
namespace match3
{
	namespace gem
	{
		static const int gemInitialPosY = screenMiddleY / 2;
		static const int gemSpacing = 5;
		const int columns = 7;
		const int rows = 8;

		gemObject boardObject[rows][columns];
		Texture2D blue;
		Texture2D red;
		Texture2D orange;
		Texture2D gray;
		//--------------------------------------------------------------------------------
		//INIT/CREATION
		static bool checkSameHorizontalGem(gemType aux, int row, int column)
		{
			return column >= 2 && boardObject[row][column - 1].type == aux
				&& boardObject[row][column - 2].type == aux;
		}
		static bool checkSameVerticalGem(gemType aux, int row, int column)
		{
			return row >= 2 && boardObject[row - 1][column].type == aux
				&& boardObject[row - 2][column].type == aux;
		}
		void loadGemTextures()
		{
			blue = LoadTexture("res/assets/textures/gameplay/blueC.png");
			blue.width = 70;
			blue.height = 490;
			frameRec = { 0.0f,0.0f,(float)blue.width,(float)blue.height / 7 };

			red = LoadTexture("res/assets/textures/gameplay/redC.png");
			red.width = 70;
			red.height = 490;
			frameRec = { 0.0f,0.0f,(float)red.width,(float)red.height / 7 };

			orange = LoadTexture("res/assets/textures/gameplay/orangeC.png");
			orange.width = 70;
			orange.height = 490;
			frameRec = { 0.0f,0.0f,(float)orange.width,(float)orange.height / 7 };

			gray = LoadTexture("res/assets/textures/gameplay/greyC.png");
			gray.width = 70;
			gray.height = 490;
			frameRec = { 0.0f,0.0f,(float)gray.width,(float)gray.height / 7 };
		}
		void createGem(gemObject& gem, int row, int column)
		{
			static gemType aux;

			do
			{
				aux = gemType(GetRandomValue(0, 3));
			} while (checkSameHorizontalGem(aux, row, column) && checkSameVerticalGem(aux, row, column));

			switch (aux)
			{
			case gemType::blue:
				gem.type = gemType::blue;
				gem.texture = blue;
				break;
			case gemType::red:
				gem.type = gemType::red;
				gem.texture = red;
				break;
			case gemType::orange:
				gem.type = gemType::orange;
				gem.texture = orange;
				break;
			case gemType::gray:
				gem.type = gemType::gray;
				gem.texture = gray;
				break;
			default:
				break;
			}

			gem.body.width = float(screenWidth / columns - gemSpacing);
			gem.body.height = float(gem.texture.height / 7);

			gem.body.x = column * gem.body.width + gemSpacing * (column + 1);
			gem.body.y = row * gem.body.height + gemSpacing * row;
		}
		void rePopulateGem(gemObject& gem, int row, int column)
		{
			static gemType aux;

			gem.active = true;
			gem.tagMoving = true;

			do
			{
				aux = gemType(GetRandomValue(0, 3));
			} while (checkSameHorizontalGem(aux, row, column));

			do
			{
				aux = gemType(GetRandomValue(0, 3));
			} while (checkSameVerticalGem(aux, row, column));

			switch (aux)
			{
			case gemType::blue:
				gem.type = gemType::blue;
				gem.texture = blue;
				break;
			case gemType::red:
				gem.type = gemType::red;
				gem.texture = red;
				break;
			case gemType::orange:
				gem.type = gemType::orange;
				gem.texture = orange;
				break;
			case gemType::gray:
				gem.type = gemType::gray;
				gem.texture = gray;
				break;
			default:
				break;
			}

			gem.body.x = float(column * gem.body.width + gemSpacing * (column + 1));
			gem.newY = row * int(gem.body.height) + gemSpacing * row;
			gem.body.y = -gem.body.height;
		}
		//--------------------------------------------------------------------------------
		//UPDATE 
		void checkHoverGem(gemObject& gem)
		{
			if (!tagSelecting)
			{
				if (CheckCollisionPointRec(mousePos, gem.body))
					gem.tagHover = true;
				else
					gem.tagHover = false;
			}
		}
		void checkSelectGem(gemObject& gem, int row, int column)
		{
			if (CheckCollisionPointRec(mousePos, gem.body) && IsMouseButtonDown(MOUSE_LEFT_BUTTON) && GetTime() > time + 1)
			{
				if (!tagSelecting)
				{
					tagSelecting = true;
					gem.tagSelected = true;

					selectedGems.push_back({ float(column),float(row) });
				}
			}
		}
		void checkAdjacentGem(gemObject& gem, int row, int column)
		{
			static Vector2 index;
			index = selectedGems.back();

			if (CheckCollisionPointRec(mousePos, gem.body) && boardObject[int(index.y)][int(index.x)].type == gem.type && !boardObject[row][column].tagSelected)
			{
				//arriba
				if (row == int(index.y) - 1 && column >= int(index.x) - 1 && column <= int(index.x) + 1)
				{
					gem.tagSelected = true;
					selectedGems.push_back({ float(column),float(row) });
				}
				//izq/der
				else if (row == int(index.y) && (column == int(index.x) - 1 || column == int(index.x) + 1))
				{
					gem.tagSelected = true;
					selectedGems.push_back({ float(column),float(row) });
				}
				//abajo
				else if (row == int(index.y) + 1 && column >= int(index.x) - 1 && column <= int(index.x) + 1)
				{
					gem.tagSelected = true;
					selectedGems.push_back({ float(column),float(row) });
				}
			}
		}
		//--------------------------------------------------------------------------------
		//DRAW/ANIMATIONS
		void drawGem(gemObject gem)
		{
			if (gem.tagHover || gem.tagSelected)
				DrawTextureRec(gem.texture, frameRec, Vector2({ float(gem.body.x + gem.texture.width / 4),float(gem.body.y) }), WHITE);
			else
				DrawTextureRec(gem.texture, Rectangle({ 0.0f,0.0f,(float)gem.texture.width,(float)gem.texture.height / 7 }), Vector2({ float(gem.body.x + gem.texture.width / 4),float(gem.body.y) }), WHITE);
#if DEBUG
			DrawRectangleLinesEx(gem.body, 2, GREEN);
#endif
		}
		void drawGemDespawn(gemObject gem)
		{
			if (despawnAnimationCounter < 2)
				DrawTextureRec(gem.texture, Rectangle({ 0.0f,350.0f,(float)gem.texture.width,(float)gem.texture.height / 7 }), Vector2({ float(gem.body.x + gem.texture.width / 4),float(gem.body.y) }), WHITE);
			else
				DrawTextureRec(gem.texture, Rectangle({ 0.0f,420.0f,(float)gem.texture.width,(float)gem.texture.height / 7 }), Vector2({ float(gem.body.x + gem.texture.width / 4),float(gem.body.y) }), WHITE);
		}
		void despawnAnimation()
		{
			if (despawnAnimationCounter > 2)
			{
				tagDespawning = false;
				tagMovingGems = true;
				despawnAnimationCounter = 0;
				animateGems();
			}
		}
		//--------------------------------------------------------------------------------
	}
}
