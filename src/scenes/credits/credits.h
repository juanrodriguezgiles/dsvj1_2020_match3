#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/main_menu/main_menu.h"
#include "objects/player/player.h"
namespace match3
{
	namespace credits
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif
