#include "credits.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace main_menu;
using namespace player;
namespace match3
{
	namespace credits
	{
		static bool firstRun = true;
		static Texture2D credits;
		static Texture2D mainMenuN;
		static Texture2D mainMenuH;
		static Rectangle mainMenuRec;
		void init()
		{
			if (firstRun)
			{
				credits = LoadTexture("res/assets/textures/credits/creditsScreen.png");
				mainMenuN = LoadTexture("res/assets/textures/howToPlay/mainMenuN.png");
				mainMenuH = LoadTexture("res/assets/textures/howToPlay/mainMenuH.png");
				mainMenuRec = { float(screenMiddleX - mainMenuN.width / 2),float(screenHeight - textSpacing),(float)mainMenuN.width,(float)mainMenuN.height };
				firstRun = false;
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, mainMenuRec) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(credits, 0, 0, WHITE);
			if (CheckCollisionPointRec(mousePos, mainMenuRec))
				DrawTexture(mainMenuH, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			else
				DrawTexture(mainMenuN, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(credits);
			UnloadTexture(mainMenuN);
			UnloadTexture(mainMenuH);
		}
	}
}