#include <iostream>
#include "gameplay.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace player;
using namespace board;
using namespace gem;
using namespace main_menu;
namespace match3
{
	namespace gameplay
	{
		static bool firstRun = true;
		static Music gameplayMusic;
		Font candy;
		Texture2D movesLeftT;
		Texture2D score;
		Texture2D highScore;

		void init()
		{
			if (firstRun)
			{
				loadGemTextures();
				gameplayMusic = LoadMusicStream("res/assets/sounds/gameplay.ogg");
				PlayMusicStream(gameplayMusic);
				SetMusicVolume(gameplayMusic, 0.04f);
				candy = LoadFont("res/assets/CANDY.TTF");
				movesLeftT = LoadTexture("res/assets/textures/gameplay/moves.png");
				score = LoadTexture("res/assets/textures/gameplay/score.png");
				highScore = LoadTexture("res/assets/textures/gameplay/highScore.png");
				match = LoadSound("res/assets/sounds/match.ogg");
				SetSoundVolume(match, 0.05f);
				noMatch = LoadSound("res/assets/sounds/noMatch.ogg");
				SetSoundVolume(noMatch, 0.01f);
				firstRun = false;
			}
			time = GetTime();
			createBoard();
			initPlayer();
		}
		void input()
		{
			if (IsKeyPressed(KEY_SPACE) && tagSelecting)
			{
				checkMatch();
			}
			if (IsMouseButtonPressed(MOUSE_RIGHT_BUTTON) && tagSelecting)
			{
				deSelectBoard();
			}
			if (IsKeyPressed(KEY_P))
			{
				PlaySound(click);
				currentScene = scene::pause;
				game::init();
			}
		}
		void update()
		{
			updateFrames();
			updateMousePos();
			updateBoard();
			UpdateMusicStream(gameplayMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			drawBackground();
			drawBoard();
			DrawTexture(score, int(screenMiddleX - score.width * 2), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%07i", playerScore), Vector2({ (float)screenMiddleX - score.width * 2 + 75 ,(float)screenHeight - textSpacing - 40 }), 25, 2, DARKBROWN);
			DrawTexture(movesLeftT, int(screenMiddleX - movesLeftT.width / 2), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%02i", movesLeft), Vector2({ (float)screenMiddleX + 45,(float)screenHeight - textSpacing - 42 }), 30, 2, DARKBROWN);
			DrawTexture(highScore, int(screenMiddleX + highScore.width), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%06i", playerHighScore), Vector2({ (float)screenMiddleX + textSpacing * 3 - 10 ,(float)screenHeight - textSpacing - 40 }), 25, 2, DARKBROWN);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(movesLeftT);
			UnloadTexture(score);
			UnloadTexture(highScore);
			UnloadSound(match);
			UnloadSound(noMatch);
			UnloadMusicStream(gameplayMusic);
			UnloadFont(candy);
		}
	}
}