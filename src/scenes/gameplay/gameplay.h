#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
#include "objects/board/board.h"
#include "objects/gem/gem.h"
#include "scenes/main_menu/main_menu.h"
namespace match3
{
	namespace gameplay
	{
		extern Font candy;
		extern Texture2D movesLeftT;
		extern Texture2D score;
		extern Texture2D highScore;

		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif