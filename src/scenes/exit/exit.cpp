#include "exit.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace player;
using namespace main_menu;
namespace match3
{
	namespace exitGame
	{
		static bool firstRun = true;
		static Texture2D mainMenu;
		static Texture2D exitPanel;
		static Texture2D exitPanelYes;
		static Texture2D exitPanelNo;
		static Rectangle exitYes;
		static Rectangle exitNo;

		void init()
		{
			if (firstRun)
			{
				mainMenu = LoadTexture("res/assets/textures/exit/menu.png");
				exitPanel = LoadTexture("res/assets/textures/exit/exit.png");
				exitPanelYes = LoadTexture("res/assets/textures/exit/exitY.png");
				exitPanelNo = LoadTexture("res/assets/textures/exit/exitN.png");
				exitYes = { float(screenMiddleX + exitPanel.width / 2 + 45),float(screenMiddleY - textSpacing + 85),60,60 };
				exitNo = { float(screenMiddleX + exitPanel.width / 2 + 145),float(screenMiddleY - textSpacing + 85),60,60 };
				firstRun = false;
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, exitYes) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				playing = false;
			}
			if (CheckCollisionPointRec(mousePos, exitNo) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			BeginDrawing();
			DrawTexture(mainMenu, 0, 1, WHITE);
			if (CheckCollisionPointRec(mousePos, exitYes))
				DrawTexture(exitPanelYes, int(screenMiddleX + exitPanel.width / 2), (int)screenMiddleY - textSpacing, WHITE);
			else if (CheckCollisionPointRec(mousePos, exitNo))
				DrawTexture(exitPanelNo, int(screenMiddleX + exitPanel.width / 2), (int)screenMiddleY - textSpacing, WHITE);
			else
				DrawTexture(exitPanel, int(screenMiddleX + exitPanel.width / 2), (int)screenMiddleY - textSpacing, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(mainMenu);
			UnloadTexture(exitPanel);
			UnloadTexture(exitPanelYes);
			UnloadTexture(exitPanelNo);
		}
	}
}