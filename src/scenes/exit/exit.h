#ifndef EXIT_H
#define EXIT_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
#include "scenes/main_menu/main_menu.h"
namespace match3
{
	namespace exitGame
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif