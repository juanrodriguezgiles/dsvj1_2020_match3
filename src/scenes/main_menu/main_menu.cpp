#include "main_menu.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace player;
namespace match3
{
	namespace main_menu
	{
		static bool firstRun = true;
		Music menuMusic;
		Sound click;
		static Texture2D buttonTitle;
		static Texture2D buttonPlay;
		static Texture2D buttonHoverPlay;
		static Texture2D buttonHowToPlay;
		static Texture2D buttonHoverHowToPlay;
		static Texture2D buttonCredits;
		static Texture2D buttonHoverCredits;
		static Texture2D buttonExit;
		static Texture2D buttonHoverExit;
		static Rectangle title;
		static Rectangle play;
		static Rectangle howToPlay;
		static Rectangle credits;
		static Rectangle exit;

		void init()
		{
			if (firstRun)
			{
				InitWindow(screenWidth, screenHeight, "MATCH3");
				SetTargetFPS(60);
				InitAudioDevice();
				initScreen();
				firstRun = false;
				menuMusic = LoadMusicStream("res/assets/sounds/mainMenu.ogg");
				PlayMusicStream(menuMusic);
				SetMusicVolume(menuMusic, 0.04f);
				click = LoadSound("res/assets/sounds/click1.ogg");
				buttonTitle = LoadTexture("res/assets/textures/mainMenu/match3Normal.png");
				buttonPlay = LoadTexture("res/assets/textures/mainMenu/playNormal.png");
				buttonHoverPlay = LoadTexture("res/assets/textures/mainMenu/playHover.png");
				buttonHowToPlay = LoadTexture("res/assets/textures/mainMenu/howToNormal.png");
				buttonHoverHowToPlay = LoadTexture("res/assets/textures/mainMenu/howToHover.png");
				buttonCredits = LoadTexture("res/assets/textures/mainMenu/creditsNormal.png");
				buttonHoverCredits = LoadTexture("res/assets/textures/mainMenu/creditsHover.png");
				buttonExit = LoadTexture("res/assets/textures/mainMenu/exitNormal.png");
				buttonHoverExit = LoadTexture("res/assets/textures/mainMenu/exitHover.png");
				title = { float(screenMiddleX - buttonTitle.width / 2),float(screenMiddleY - textSpacing * 3),float(buttonTitle.width),float(buttonTitle.height) };
				play = { float(screenMiddleX - buttonPlay.width / 2),float(screenMiddleY - textSpacing),float(buttonPlay.width),float(buttonPlay.height) };
				howToPlay = { float(screenMiddleX - buttonPlay.width / 2),float(screenMiddleY),float(buttonPlay.width),float(buttonPlay.height) };
				credits = { float(screenMiddleX - buttonPlay.width / 2),float(screenMiddleY + textSpacing),float(buttonPlay.width),float(buttonPlay.height) };
				exit = { float(screenMiddleX - buttonPlay.width / 2),float(screenMiddleY + textSpacing * 2),float(buttonPlay.width),float(buttonPlay.height) };
			}

		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, play) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::gameplay;
				game::init();
			}
			if (CheckCollisionPointRec(mousePos, howToPlay) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::how_to_play;
				game::init();
			}
			if (CheckCollisionPointRec(mousePos, credits) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::credits;
				game::init();
			}
			if (CheckCollisionPointRec(mousePos, exit) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::exitGame;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(backgroundMainMenu, 0, 0, WHITE);
			DrawTexture(buttonTitle, (int)title.x, (int)title.y, WHITE);
			if (CheckCollisionPointRec(mousePos, play))
				DrawTexture(buttonHoverPlay, (int)play.x, (int)play.y, WHITE);
			else
				DrawTexture(buttonPlay, (int)play.x, (int)play.y, WHITE);
			if (CheckCollisionPointRec(mousePos, howToPlay))
				DrawTexture(buttonHoverHowToPlay, (int)howToPlay.x, (int)howToPlay.y, WHITE);
			else
				DrawTexture(buttonHowToPlay, (int)howToPlay.x, (int)howToPlay.y, WHITE);
			if (CheckCollisionPointRec(mousePos, credits))
				DrawTexture(buttonHoverCredits, (int)credits.x, (int)credits.y, WHITE);
			else
				DrawTexture(buttonCredits, (int)credits.x, (int)credits.y, WHITE);
			if (CheckCollisionPointRec(mousePos, exit))
				DrawTexture(buttonHoverExit, (int)exit.x, (int)exit.y, WHITE);
			else
				DrawTexture(buttonExit, (int)exit.x, (int)exit.y, WHITE);
			DrawText("V0.2.2", 5, screenHeight - 20, 20, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(buttonTitle);
			UnloadTexture(buttonPlay);
			UnloadTexture(buttonHoverPlay);
			UnloadTexture(buttonHowToPlay);
			UnloadTexture(buttonHoverHowToPlay);
			UnloadTexture(buttonCredits);
			UnloadTexture(buttonHoverCredits);
			UnloadSound(click);
			UnloadMusicStream(menuMusic);
		}
	}
}