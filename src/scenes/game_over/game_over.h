#ifndef GAME_OVER_H
#define GAME_OVER_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/main_menu/main_menu.h"
#include "objects/board/board.h"
#include "objects/player/player.h"
namespace match3
{
	namespace game_over
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif