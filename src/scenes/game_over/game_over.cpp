#include "game_over.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace gameplay;
using namespace main_menu;
using namespace player;
using namespace board;
namespace match3
{
	namespace game_over
	{
		static bool firstRun = true;
		static Texture2D gameOverPanel;

		void init()
		{
			if (firstRun)
			{
				gameOverPanel = LoadTexture("res/assets/textures/gameOver/gameOver.png");
				firstRun = false;
			}
		}
		void input()
		{
			if (IsKeyPressed(KEY_P))
			{
				PlaySound(click);
				currentScene = scene::gameplay;
				game::init();
			}
			if (IsKeyPressed(KEY_M))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
			deSelectBoard();
		}
		void update()
		{

		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			drawBackground();
			drawBoard();
			DrawTexture(score, int(screenMiddleX - score.width * 2), int(screenHeight - textSpacing), WHITE);
			DrawTextEx(candy, FormatText("%07i", playerScore), Vector2({ (float)screenMiddleX - score.width * 2 + 75 ,(float)screenHeight - textSpacing + 10 }), 25, 2, DARKBROWN);
			DrawTexture(movesLeftT, int(screenMiddleX - movesLeftT.width / 2), int(screenHeight - textSpacing), WHITE);
			DrawTextEx(candy, FormatText("%02i", movesLeft), Vector2({ (float)screenMiddleX + 45,(float)screenHeight - textSpacing + 7 }), 30, 2, DARKBROWN);
			DrawTexture(highScore, int(screenMiddleX + highScore.width), int(screenHeight - textSpacing), WHITE);
			DrawTextEx(candy, FormatText("%06i", playerHighScore), Vector2({ (float)screenMiddleX + textSpacing * 3 - 10 ,(float)screenHeight - textSpacing + 10 }), 25, 2, DARKBROWN);
			DrawTexture(gameOverPanel, int(screenMiddleX - gameOverPanel.width / 2), (int)screenMiddleY - textSpacing * 2, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(gameOverPanel);
		}
	}
}