#include "pause.h"
using namespace match3;
using namespace game;
using namespace screen;
using namespace player;
using namespace board;
using namespace gem;
using namespace main_menu;
using namespace gameplay;
namespace match3
{
	namespace pause
	{
		static bool firstRun = true;
		static Texture2D pausePanel;

		void init()
		{
			if (firstRun)
			{
				pausePanel = LoadTexture("res/assets/textures/pause/pause.png");
				firstRun = false;
			}
		}
		void input()
		{
			if (IsKeyPressed(KEY_P))
			{
				PlaySound(click);
				currentScene = scene::gameplay;
			}
			if (IsKeyPressed(KEY_M))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			drawBackground();
			drawBoard();
			DrawTexture(score, int(screenMiddleX - score.width * 2), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%07i", playerScore), Vector2({ (float)screenMiddleX - score.width * 2 + 75 ,(float)screenHeight - textSpacing - 40 }), 25, 2, DARKBROWN);
			DrawTexture(movesLeftT, int(screenMiddleX - movesLeftT.width / 2), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%02i", movesLeft), Vector2({ (float)screenMiddleX + 45,(float)screenHeight - textSpacing - 42 }), 30, 2, DARKBROWN);
			DrawTexture(highScore, int(screenMiddleX + highScore.width), int(screenHeight - textSpacing - 50), WHITE);
			DrawTextEx(candy, FormatText("%06i", playerHighScore), Vector2({ (float)screenMiddleX + textSpacing * 3 - 10 ,(float)screenHeight - textSpacing - 40 }), 25, 2, DARKBROWN);
			DrawTexture(pausePanel, int(screenMiddleX - pausePanel.width / 2), (int)screenMiddleY - textSpacing * 2, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(pausePanel);
		}
	}
}