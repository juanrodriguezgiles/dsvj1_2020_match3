#ifndef HOW_TO_PLAY_H
#define HOW_TO_PLAY_H
#include "raylib.h"
#include "game/game.h"
#include "objects/player/player.h"
#include "scenes/main_menu/main_menu.h"
namespace match3
{
	namespace how_to_play
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif