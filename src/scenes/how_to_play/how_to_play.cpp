#include "how_to_play.h"
using namespace match3;
using namespace game;
using namespace player;
using namespace main_menu;
namespace match3
{
	namespace how_to_play
	{
		static bool firstRun = true;
		static Texture2D tutorial;
		static Texture2D mainMenuN;
		static Texture2D mainMenuH;
		static Rectangle mainMenuRec;
		void init()
		{
			if (firstRun)
			{
				tutorial = LoadTexture("res/assets/textures/howToPlay/gameplayScreen.png");
				mainMenuN = LoadTexture("res/assets/textures/howToPlay/mainMenuN.png");
				mainMenuH = LoadTexture("res/assets/textures/howToPlay/mainMenuH.png");
				mainMenuRec = { 100,100,(float)mainMenuN.width,(float)mainMenuN.height };
				firstRun = false;
			}
		}
		void input()
		{
			if (CheckCollisionPointRec(mousePos, mainMenuRec) && IsMouseButtonDown(MOUSE_LEFT_BUTTON))
			{
				PlaySound(click);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			updateMousePos();
			UpdateMusicStream(menuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(tutorial, 0, 0, WHITE);
			if (CheckCollisionPointRec(mousePos, mainMenuRec))
				DrawTexture(mainMenuH, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			else
				DrawTexture(mainMenuN, (int)mainMenuRec.x, (int)mainMenuRec.y, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			UnloadTexture(tutorial);
			UnloadTexture(mainMenuN);
			UnloadTexture(mainMenuH);
		}
	}
}